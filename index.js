// Les constantes déclarées
const header = document.getElementById("header");
const content = document.getElementById("content");

function getJoke() {
  // permet d'aller chercher le url
  fetch("https://api.blablagues.net/?rub=blagues")
    //   Récupérer et afficher les blague dans la div app: header: la blague et content: la réponse
    .then((res) => res.json())
    .then((data) => {
      const joke = data.data.content;

      header.textContent = joke.text_head;

      content.textContent = joke.text !== "" ? joke.text : joke.text_hidden;
    });
}
// Lance la fonction
getJoke();

// Permet de faire un clique sur le body pour passer à la blague suivante
document.body.addEventListener("click", getJoke);
